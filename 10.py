#!/usr/local/bin/python3.9

def funcao1() :
    print("Esta funcao nao recebe nenhum argumento")
    print("Esta funcao nao retorna nenhum valor")

def funcao2():
    print("Esta funcao nao recebe nenhum argumento")
    print("Esta funcao retorna um valor")
    return 4

def funcao3(a):
    print("Esta funcao recebe um argumento",a)
    print("Esta funcao retorna um valor")
    return a+5

def troca(a,b) :
    return b,a

def maior(a,b,c):
    if ( a> b and a>c):
        return a
    else:
        if ( b>a  and b > c ):
            return b
        else:
            return c

print("funcao1")
funcao1()
print("funcao2")
print(funcao2())
print("funcao3")
print(funcao3(20))
print("Valores 2 e 3", troca(2,3))
print("2 4 0 - ",maior(2,4,0))


