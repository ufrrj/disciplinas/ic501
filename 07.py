#!/usr/local/bin/python3.9

num = -1
while ( num < 0 ):
    num = int(input("Digite um valor positivo inteiro: "))

s = "Impar"
if ( num % 2 == 0 ):
    s = "Par"

print("O numero", num, "e",s)
