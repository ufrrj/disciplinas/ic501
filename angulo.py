#!/usr/local/bin/python3.9
'''
    Calcular o Angulo com as seguintes informações:

    Hipotenusa
    Cateto
    Se é cateto oposto ou se é adjacente

1. Definir variaveis
    1.1, a,h,c,r
2. Mostrar "Digite a Hipotenusa: "
3. Ler h
4. Mostrar "Digite o valor do cateto: "
5. Ler c
6. Enquanto r < 1 ou r > 2 Faça
    6.1. Mostrar "Digite 1 para Oposto ou 2 para Adjacente"
    6.2. Ler r
7. Se ( r == 1 ) Entção
    7.1 a = arcsin(c/h)
8. Caso contrario
    8.1 a = arccos(c/h)
9. Mostrar "Angulo = ", a, "radianos"
10. MOstrar "Angulo = ", a * 180/PI, "Graus"
11. Fim
X - Y
180 - PI
X = Y * 180/PI
Y = x * PI/180

'''
import math

h = float(input("Digite a hipotenusa: "))
c = float(input("Digite o cateto: "))
r = -1
while ( r < 1 or r > 2 ):
    r = int(input("Digite 1 para oposto ou 2 para adjacente"))

if ( r == 1 ):
    a = math.asin(c/h)
else :
    a = math.acos(c/h)

print("Angulo =", a, "radianos")
print("Angulo =", a*180/3.14159, "graus")
print("Angulo =", a*180/math.pi, "graus")

c = math.sqrt(2)
print(c)
a = math.asin(c/h)

print("Angulo =", a, "radianos")
print("Angulo =", a*180/3.14159, "graus")
print("Angulo =", a*180/math.pi, "graus")

