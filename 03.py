#!/usr/local/bin/python3.9
'''
    Comentário de bloco
    pode conter várias linhas sem necessidade de quebrar

'''
# comentário de uma única linha

# Comando LER do algoritimo
# o comando input é o comando LER associado ao 
# comando MOSTRAR
numero = int(input("Digite numero inteiro: "))  
# int() converte string para inteiro
numero2 = float(input("Digite um numero com casas decimais: "))
# float() converte string para ponto flutuante ( numeros reais )
# comando MOSTRAR
print("O número digitado é: ", numero )
print("O número digitado é: ", numero2 )

print("Soma.............: ",  numero, " + 1     =", numero   + 1   )
print("Soma.............: ", numero2, " + 1     =", numero2  + 1   )
print("Subtraçào........: ",  numero, " - 3     =", numero   - 3   )
print("Subtraçào........: ", numero2, " - 3     =", numero2  - 3   )
print("Multiplicaçào....: ",  numero, " * 10    =", numero   * 10  )
print("Multiplicaçào....: ", numero2, " * 10    =", numero2  * 10  )
print("Divisào..........: ",  numero, " / 100   =", numero   / 100 )
print("Divisào..........: ", numero2, " / 100   =", numero2  / 100 )
print("Divisào inteira..: ",  numero, " // 3    =", numero   // 3  )
print("Divisào inteira..: ", numero2, " // 3    =", numero2  // 3  )
print("Resto da divisào.: ",  numero, " % 3     =", numero   % 3   )
print("Resto da divisào.: ", numero2, " % 3     =", numero2  % 3   )
