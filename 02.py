#!/usr/local/bin/python3.9
'''
    Comentário de bloco
    pode conter várias linhas sem necessidade de quebrar

'''
# comentário de uma única linha

# Comando LER do algoritimo
# o comando input é o comando LER associado ao 
# comando MOSTRAR
nome = input("Digite seu nome: ")

# comando MOSTRAR
print("Seu nome é: ", nome)
