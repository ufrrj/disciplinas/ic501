#!/usr/local/bin/python3.9

a = int(input("Digite um valor: "))
b = int(input("Digite um valor: "))

if ( a > b ) :
        print (a, " > " , b )  # comandos caso a expressào seja verdadeira
else :
        print ( a, " <= ", b ) # comandos caso a expressào seja falsa

# IF's aninhados
if ( a > b ) :
    print(">")
else :
    if ( a < b ) :
        print ( "<" )
    else :
        print( "=" )


