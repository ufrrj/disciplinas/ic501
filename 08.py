#!/usr/local/bin/python3.9

import math

print("Programa para calcular as raizes de uma equacao do segundo grau")

a = int(input("Digite o valor para a variavel A da equacao: "))
b = int(input("Digite o valor para a variavel B da equacao: "))
c = int(input("Digite o valor para a variavel C da equacao: "))

x1 = 0
x2 = 0

delta = b*b - 4 * a * c
if ( delta < 0 ) :
    print("Raizes nao reais")
else :
    x1 = (-b + math.sqrt(delta))/(2*a)
    x2 = x1
    if ( delta > 0 ) :
        x2 = ( - b - math.sqrt(delta))/(2*a)
    print("As raizes da equacao:",a,"x^2 +",b,"x +",c)
    print(x1)
    print(x2)
