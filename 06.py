#!/usr/local/bin/python3.9
'''
    Comentário de bloco
    pode conter várias linhas sem necessidade de quebrar

'''

#FAÇA  VAR <- Vi ATÉ Vf [ PASSO P] 

nome = input("Digite um nome: ")
# Pega letra a letra do nome
for letra in nome :
    print("Letra", letra)

# contador de 1 ate 10
# o comando range cria uma lista do primeiro valor ate
# o valor final - 1
# para contar de 1 ate 10 preciso criar uma lista com
# range(1,11)
for n in range(1,11):
    print(n)

print(list(range(1,11)))
print(list(range(1,11,2)))
print(list(range(11,1,-1)))
print(list(range(10,0,-1)))
