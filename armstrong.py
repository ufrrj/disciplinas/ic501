#!/usr/local/bin/python3.9

num = input("Digite um valor: ")
potencia=len(num)
soma = 0
for digito in num:
    soma = soma + int(digito) ** potencia

if ( soma == int(num) ):
    print("Numero Armstrong")
else:
    print("Tente novamente")


contador = 100
contador_arm=0
while ( contador <= 1000000000 ):
    num = str(contador)
    potencia=len(num)
    soma = 0
    for digito in num:
        soma = soma + int(digito) ** potencia
        contador_arm = contador_arm + 1

    if ( soma == int(num) ):
        print(num, "Numero Armstrong")
    contador = contador + 1

print("Total de numeros armstrong: ", contador_arm)
