#!/usr/local/bin/python3.9

def menu():
    print("Escolha a opcao desejada")
    print("")
    print("   1 - Triangulo")
    print("   2 - Retangulo")
    print("   3 - Circulo")
    print("   0 - Terminar")
    r = int(input("DIgite a opcao: "))
    return r

opcao = -1
while ( opcao != 0 ):
    opcao = menu()
    print("Voce escolheu a opcao:", opcao)
