#!/usr/local/bin/python3.9

'''
    Escopo de variaveis

    Variaveis GLOBAIS
    Variaveis LOCAIS
'''

def funcao0():
    b = 2 
    print("Funcao 0 =",a)

a = 1

def funcao1():
    global a
    a = a + 3 
    print("Funcao 1 =",a)


print(a)
funcao0()
print(a)
funcao1()
print(a)
if ( a > 1 ):
    c = 2
else:
    c = 3
print(c)
