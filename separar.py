#!/usr/local/bin/python3.9

'''
1. Definir variáveis
	1.1 frase, contador, tam, palavra
2. Mostrar "Digite uma frase"
3. Ler frase
4. tam <- comprimento( frase )
5. contador <- 0
6. Enquanto ( contador < tam ) FAÇA
	6.1. Se ( frase[contador] = " " )
		6.1.1. Mostrar palavra
		6.1.2. palavra <- ""
	6.2. Caso contrário
		6.2.1 palavra <- palavra + frase[contador]
	contador <- contador + 1
7. Fim
'''

frase = input("Digite uma frase: ")
tam = len(frase)
contador = 0
palavra = ""
while ( contador < tam ):
	if ( frase[contador] == " " ):
		print( palavra )
		palavra = ""
	else :
		palavra = palavra + frase[contador]
	contador = contador + 1
print(palavra)
