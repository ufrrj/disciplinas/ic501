#!/usr/local/bin/python3.9
'''
    Comentário de bloco
    pode conter várias linhas sem necessidade de quebrar

'''

print("Tabela verdade")

print("|    AND    |" )
print("| A | B | S |")
print("| 0 | 0 |", (0 and 0), "|")
print("| 0 | 1 |", (0 and 1), "|")
print("| 1 | 0 |", (1 and 0), "|")
print("| 1 | 1 |", (1 and 1), "|")
print("")
print("|     OR    |" )
print("| A | B | S |")
print("| 0 | 0 |", (0 or 0), "|")
print("| 0 | 1 |", (0 or 1), "|")
print("| 1 | 0 |", (1 or 0), "|")
print("| 1 | 1 |", (1 or 1), "|")
print("")
print("|    XOR    |" )
print("| A | B | S |")
print("| 0 | 0 |", ((0 and not(0)) or (not(0) and 0)), "|")
print("| 0 | 1 |", ((0 and not(1)) or (not(0) and 1)), "|")
print("| 1 | 0 |", int(((1 and not(0)) or (not(1) and 0))), "|")
print("| 1 | 1 |", int(((1 and not(1)) or (not(1) and 1))), "|")
print("")
print("|    NOT    |" )
print("| A | S |")
print("| 0 |", int(not(0)), "|")
print("| 1 |", int(not(1)), "|")
